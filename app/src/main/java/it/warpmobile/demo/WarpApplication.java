package it.warpmobile.demo;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;



/**
 * @author Giuseppe Sorce
 *         <p/>
 *         <p/>
 *         WarpApplication extends Application. it helps to create a Singleton to store
 *         Volley instance. Volley is a library to use http, https request.
 */
public class WarpApplication extends Application {

    private static WarpApplication sInstance;
    private String macaddress = "";
    private RequestQueue queue;

    public static WarpApplication get() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        queue = Volley.newRequestQueue(this);
    }

    public RequestQueue getVolley() {
        return queue;
    }


}