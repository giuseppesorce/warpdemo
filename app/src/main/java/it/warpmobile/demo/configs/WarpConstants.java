package it.warpmobile.demo.configs;

/**
 * Created by devandroid on 18/08/14.
 */
public class WarpConstants {

    public static class PATH{

        public static String URL_JSON="https://gdata.youtube.com/feeds/api/videos?q=androiddevelopers&max-results=20&v=2&alt=jsonc";


    }

    public static class FragmentsType{

        public static final int HOME=1;
        public static final int VIDEO_LIST=2;
    }
}
