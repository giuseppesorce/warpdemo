// Copyright 2012 Google Inc. All Rights Reserved.

package it.warpmobile.demo.youtube;

/**
 * Static container class for holding a reference to your YouTube Developer Key.
 */
public class DeveloperKey {

  /**
   * Please replace this with a valid API key which is enabled for the 
   * YouTube Data API v3 service. Go to the 
   * <a href="https://code.google.com/apis/console/">Google APIs Console</a> to
   * register a new developer key.
   */
//public static final String DEVELOPER_KEY = "AIzaSyCu-HuuK5LNy0dTCtzCmV6NpZLjJx6Il60";
  public static final String DEVELOPER_KEY = "AIzaSyAXms04cR6BkmgH4NbanonJrRBi7HEJbXQ";


}
