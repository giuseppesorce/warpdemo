package it.warpmobile.demo.comm.responses;

/**
 * Created by devandroid on 11/02/14.
 */


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import it.warpmobile.demo.modeldata.VideoItem;

public class ResponseVideoList extends ResponseGeneric {

    @SerializedName("data")
    public Data data;

    public class Data{
        @SerializedName("items")
        public ArrayList<VideoItem> items;
    }

}
