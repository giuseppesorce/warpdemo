package it.warpmobile.demo.comm.responses;

/**
 * Created by Giuseppe Sorce  on 24/11/13.
 */
public class ResponseServer<TResponse> {

    private int status;
    private String rawResponse;
    private TResponse response;
    private String error;
    public ResponseServer( TResponse response) {


        this.response = response;
    }

    public TResponse getResponse(){
        return response;
    }
}
