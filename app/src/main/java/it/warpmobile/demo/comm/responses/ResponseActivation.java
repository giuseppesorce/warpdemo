package it.warpmobile.demo.comm.responses;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Giuseppe Sorce  on 23/11/13.
 */
public class ResponseActivation extends ResponseGeneric {


    @SerializedName("data")
    public StatusResponse data;
    public class StatusResponse{


        //        public String server;
        public int activated;
        public int timetocheck;



    }
}
