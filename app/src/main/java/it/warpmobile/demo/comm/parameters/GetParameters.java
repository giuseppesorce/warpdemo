package it.warpmobile.demo.comm.parameters;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class GetParameters {
	
	private List<NameValuePair> params = new ArrayList<NameValuePair>();
	
	public void addParam(String name, String value){
		params.add(new BasicNameValuePair(name, value));
	}

	public List<NameValuePair> getParams(){
		return params;
	}

    public NameValuePair get(int o){
        return params.get(o);
    }

    public int size(){
        return params.size();
    }

}
