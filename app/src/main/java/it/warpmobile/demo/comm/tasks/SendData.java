package it.warpmobile.demo.comm.tasks;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import it.warpmobile.demo.WarpApplication;
import it.warpmobile.demo.comm.listeners.ResponseListener;
import it.warpmobile.demo.comm.parameters.GetParameters;
import it.warpmobile.demo.comm.parameters.PostParameters;
import it.warpmobile.demo.comm.requests.JSonRequest;
import it.warpmobile.demo.comm.responses.ResponseServer;
import it.warpmobile.demo.debug.Dbg;
import it.warpmobile.demo.modeldata.CPError;

/**
 * Created by Giuseppe Sorce  on 23/11/13.
 */
public class SendData<TPayload, TResponse> implements Runnable {

    private final Context mContext;
    private final ResponseListener responseListener;
    private JSonRequest requetParameter;
    private PostParameters postparameters = null;
    private GetParameters getparameters = null;
    private Class<TResponse> mResponseClass;
    private Gson gson = new Gson();


    public SendData(Context context, ResponseListener<TResponse> listener, JSonRequest request) {
        mContext = context;
        responseListener = listener;
        requetParameter = request;
        mResponseClass = request.mResponseClass;
    }

    public void run() {

        String json = gson.toJson(requetParameter.mPayload);
        JSONObject jsonO = null;
        try {
            jsonO = new JSONObject(json);
        } catch (JSONException e) {

        }
        Dbg.p("SEND URL: " + requetParameter.getUrl(), true);


        JsonObjectRequest jsonr = new JsonObjectRequest(Request.Method.GET,
                requetParameter.getUrl(), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        sendResponse(response);

                        ;
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Dbg.p(error.getMessage());
            }
        });

        WarpApplication.get().getVolley().add(jsonr);


    }

    private void sendResponse(JSONObject json) {

        Dbg.p("JSON Response: " + json.toString(), true);
        TResponse tresp = null;
        try {


            tresp = gson.fromJson(json.toString(), mResponseClass);
        } catch (JsonSyntaxException ex) {
            Dbg.p("JSON Response Error Syntax");
        }


        if (tresp != null) {

            ResponseServer<TResponse> mResponse = new ResponseServer<TResponse>(tresp);
            responseListener.onServerResponse(mResponse);
        } else {

        }
    }

}