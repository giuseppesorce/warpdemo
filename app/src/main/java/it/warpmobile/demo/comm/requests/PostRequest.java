package it.warpmobile.demo.comm.requests;


import it.warpmobile.demo.comm.parameters.PostParameters;

/**
 * Created by Giuseppe Sorce  on 24/11/13.
 */
public class PostRequest<TResponse> {
    private final String urlRequest;
    public PostParameters paramenters;
    public Class<TResponse> mResponseClass;

    public PostRequest(String url, Class<TResponse> response, PostParameters pars) {

        urlRequest= url;
        mResponseClass = response;
        paramenters=pars;

    }

    public String getUrl(){
        return urlRequest;
    }
}
