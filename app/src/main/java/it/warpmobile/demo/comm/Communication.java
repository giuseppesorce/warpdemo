package it.warpmobile.demo.comm;

import android.content.Context;


import it.warpmobile.demo.comm.listeners.CommListener;
import it.warpmobile.demo.comm.listeners.ResponseListener;
import it.warpmobile.demo.comm.parameters.PostParameters;
import it.warpmobile.demo.comm.requests.JSonRequest;
import it.warpmobile.demo.comm.requests.modelrequest.VideoListRequest;
import it.warpmobile.demo.comm.responses.ResponseGeneric;
import it.warpmobile.demo.comm.responses.ResponseServer;
import it.warpmobile.demo.comm.responses.ResponseVideoList;
import it.warpmobile.demo.configs.WarpConstants;
import it.warpmobile.demo.modeldata.CPError;


/**
 * Created by Giuseppe Sorce  on 23/11/13.
 */
public class Communication {

    private final Context mContext;

    public ResponseListener<ResponseGeneric> responselistener = new ResponseListener<ResponseGeneric>() {


        public void onServerResponse(ResponseServer<ResponseGeneric> response) {

            activeListener.onGetData(response.getResponse());

        }


        public void onServerResponseError(ResponseServer<ResponseGeneric> response) {

        }


        public void onError(CPError errore) {

        }
    };

    private CommListener activeListener;

    private PostParameters parameters;

    public Communication(Context context) {
        mContext = context;
    }

    public void getVideoList(CommListener listener) {
        activeListener = listener;

        CommService service = new CommService();
        VideoListRequest status = new VideoListRequest();
        JSonRequest<VideoListRequest, ResponseVideoList> req = new JSonRequest<VideoListRequest, ResponseVideoList>(WarpConstants.PATH.URL_JSON, status, ResponseVideoList.class);
        service.sendTaskRequest(mContext, responselistener, req);

    }


}
