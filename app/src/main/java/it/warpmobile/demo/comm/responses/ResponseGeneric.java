package it.warpmobile.demo.comm.responses;


import it.warpmobile.demo.modeldata.CPError;

/**
 * Created by devandroid on 15/03/14.
 */
public class ResponseGeneric<TResponse> {

    public long timestamp;

    public String messageType;

    public CPError error;

    
}
