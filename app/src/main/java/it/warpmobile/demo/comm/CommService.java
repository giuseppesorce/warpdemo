package it.warpmobile.demo.comm;

import android.content.Context;


import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import it.warpmobile.demo.comm.listeners.ResponseListener;
import it.warpmobile.demo.comm.requests.JSonRequest;
import it.warpmobile.demo.comm.tasks.SendData;


public class CommService<TPayload, Tresponse> {

	private static final int MAXIMUM_POOL_SIZE = 10;
	private static final BlockingQueue<Runnable> sWorkQueue = new LinkedBlockingQueue<Runnable>(
			MAXIMUM_POOL_SIZE);;

    public void sendTaskRequest(Context context, ResponseListener<Tresponse> listener, JSonRequest request) {
        SendData<TPayload, Tresponse> task = new SendData<TPayload, Tresponse>(context, listener, request);

        ThreadPoolExecutor executor = new ThreadPoolExecutor(3, 5, 0L,
                TimeUnit.MILLISECONDS, sWorkQueue);
        executor.execute(task);
    }
}