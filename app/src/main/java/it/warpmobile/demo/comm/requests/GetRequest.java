package it.warpmobile.demo.comm.requests;

import org.apache.http.NameValuePair;

import it.warpmobile.demo.comm.parameters.GetParameters;

/**
 * Created by Giuseppe Sorce  on 24/11/13.
 */
public class GetRequest<TResponse> {
    public GetParameters paramenters;
    public Class<TResponse> mResponseClass;
    private String urlRequest;

    public GetRequest(String url, Class<TResponse> response, GetParameters pars) {

        urlRequest = url;
        mResponseClass = response;
        paramenters = pars;

    }

    public String getUrl() {

        String url=urlRequest;
        int size= paramenters.size();
        for(int i=0; i<size; i++){
            NameValuePair pa = paramenters.get(i);
            url+= pa.getName()+"="+pa.getValue()+"&";
        }
        return url;

    }
}
