package it.warpmobile.demo.comm.listeners;


import it.warpmobile.demo.comm.responses.ResponseTask;

/**
 * Created by Giuseppe Sorce  on 25/11/13.
 */
public interface DataListener {

    public void onResponse(ResponseTask.ResponseData data);
    public void onError();
}
