package it.warpmobile.demo.comm.requests;




/**
 * Created by Giuseppe Sorce  on 24/11/13.
 */
public class JSonRequest<TPayload, TResponse> {
    private final String urlRequest;

    public TPayload mPayload;
    public Class<TResponse> mResponseClass;

    public JSonRequest(String url, TPayload payload, Class<TResponse> response) {

        urlRequest= url;
        mResponseClass = response;
        mPayload= payload;


    }

    public String getUrl(){
        return urlRequest;
    }
}
