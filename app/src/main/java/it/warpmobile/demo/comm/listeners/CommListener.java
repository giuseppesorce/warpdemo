package it.warpmobile.demo.comm.listeners;


import it.warpmobile.demo.comm.responses.ResponseGeneric;
import it.warpmobile.demo.modeldata.CPError;

/**
 * Created by Giuseppe Sorce  on 23/11/13.
 */
public interface CommListener {

    public void onGetData(ResponseGeneric info);

    public void onError(CPError error);
    public void onServerResponseError(CPError error);
}
