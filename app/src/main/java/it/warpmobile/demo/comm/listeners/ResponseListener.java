package it.warpmobile.demo.comm.listeners;


import it.warpmobile.demo.comm.responses.ResponseServer;
import it.warpmobile.demo.modeldata.CPError;

public interface ResponseListener<Tresponse>  {


    public void onServerResponse(ResponseServer<Tresponse> response);


	public void onServerResponseError(ResponseServer<Tresponse> response);
	public void onError(CPError errore);

	
}

