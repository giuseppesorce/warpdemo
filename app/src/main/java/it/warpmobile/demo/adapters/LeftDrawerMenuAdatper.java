package it.warpmobile.demo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import it.warpmobile.demo.R;
import it.warpmobile.demo.modeldata.ItemDrawer;


public class LeftDrawerMenuAdatper extends ArrayAdapter<ItemDrawer> {

    private int resource;
    private LayoutInflater inflater;
    private Context mycontext;

    public LeftDrawerMenuAdatper(Context context, int resourceId,
                                 List<ItemDrawer> objects) {
        super(context, resourceId, objects);
        mycontext = context;
        resource = resourceId;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ViewHolder vh;
        if (view == null) {
            vh = new ViewHolder();
            view = inflater.inflate(R.layout.row, null);
            vh.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            vh.tvIcon = (ImageView) view.findViewById(R.id.ivIcon);

            view.setTag(vh);
        } else {
            vh = (ViewHolder) view.getTag();
        }

        ItemDrawer item = getItem(position);

        vh.tvTitle.setText(item.getTitle());
        vh.tvIcon.setImageDrawable(item.getIcon());
        return view;

    }

    class ViewHolder {
        TextView tvTitle;
        ImageView tvIcon;
    }

}
