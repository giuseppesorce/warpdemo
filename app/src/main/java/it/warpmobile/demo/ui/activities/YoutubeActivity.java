package it.warpmobile.demo.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import it.warpmobile.demo.R;
import it.warpmobile.demo.debug.Dbg;
import it.warpmobile.demo.modeldata.VideoItem;
import it.warpmobile.demo.utils.SimpleRegExp;
import it.warpmobile.demo.utils.WMDate;
import it.warpmobile.demo.youtube.DeveloperKey;

/**
 * Created by devandroid on 19/08/14.
 */
public class YoutubeActivity extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener {


    private String idtoplay;
    private YouTubePlayer myplayer;
    private VideoItem videoItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playeractivity);

        YouTubePlayerView youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);

        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvDescription = (TextView) findViewById(R.id.tvDescription);
        TextView tvUpdate = (TextView) findViewById(R.id.tvUpdate);
        youTubeView.initialize(DeveloperKey.DEVELOPER_KEY, this);

        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            videoItem = (VideoItem) intent.getSerializableExtra("video");

        }
        if (videoItem != null) {
            tvTitle.setText(videoItem.title);
            tvDescription.setText(videoItem.description);
            tvUpdate.setText(WMDate.getTextDate(videoItem.updated));
            idtoplay = videoItem.id;
        }

        getActionBar().setDisplayShowCustomEnabled(true);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case android.R.id.home :

               closeActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void closeActivity() {

        finish();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            if (idtoplay.length() > 0) {
                myplayer = player;
                player.cueVideo(idtoplay);


            }
        }
    }


    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Error Init Youtube Player", Toast.LENGTH_LONG).show();

    }
}
