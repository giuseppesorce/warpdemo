package it.warpmobile.demo.ui.widgets.utils;


import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.Button;
import android.widget.TextView;

import it.warpmobile.demo.R;


public class UtilFont {
    public static Map<String, Typeface> typefaceCache = new HashMap<String, Typeface>();

    public static void setTypeface(TextView textView, String typefaceName) {
        Context context = textView.getContext();      
      
 
        if (typefaceCache.containsKey(typefaceName)) {
            textView.setTypeface(typefaceCache.get(typefaceName));
        } else {
        	
            Typeface typeface;
            try {
                typeface = Typeface.createFromAsset(textView.getContext().getAssets(), context.getString(R.string.assets_fonts_folder) + typefaceName);
            } catch (Exception e) {
             
                return;
            }

            typefaceCache.put(typefaceName, typeface);
            textView.setTypeface(typeface);
        }
    }
    
    public static void setTypeface(Button button, String typefaceName) {
        Context context = button.getContext();

      
      

        if (typefaceCache.containsKey(typefaceName)) {
        	button.setTypeface(typefaceCache.get(typefaceName));
        } else {
            Typeface typeface;
            try {
                typeface = Typeface.createFromAsset(button.getContext().getAssets(), context.getString(R.string.assets_fonts_folder) + typefaceName);
            } catch (Exception e) {
             
                return;
            }

            typefaceCache.put(typefaceName, typeface);
            button.setTypeface(typeface);
        }
    }
    
    public static Typeface getTypeFace(Context context, String typefaceName){
    	

        if (typefaceCache.containsKey(typefaceName)) {
        	return (typefaceCache.get(typefaceName));
        } else {
            Typeface typeface;
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.assets_fonts_folder) + typefaceName);
              
            } catch (Exception e) {
             
                return null;
            }

            typefaceCache.put(typefaceName, typeface);
            return typeface;
          
        }
    	
    }
    
}