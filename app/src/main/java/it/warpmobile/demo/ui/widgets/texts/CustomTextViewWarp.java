package it.warpmobile.demo.ui.widgets.texts;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;


import it.warpmobile.demo.R;
import it.warpmobile.demo.ui.widgets.utils.UtilFont;


/**
 * public class CustomTestWarp extends TextView  is a custom TextView. Class set and cache font programmatically
 */
public class CustomTextViewWarp extends TextView {

    String font;

    public CustomTextViewWarp(Context context) {
        super(context);
    }

    public CustomTextViewWarp(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (!isInEditMode()) {
            TypedArray styles_array = context.obtainStyledAttributes(attrs, R.styleable.CustomTextWarp);
            font = styles_array.getString(R.styleable.CustomTextWarp_font);
            setFont();
        }
    }


    public void setFont(String f) {
        font = f;
        setFont();
    }

    private void setFont() {
        if (font.equals("roboto")) {
            UtilFont.setTypeface(this, "Roboto-Regular.ttf");
        } else if (font.equals("robotobold")) {

            UtilFont.setTypeface(this, "Roboto-Bold.ttf");
        }

    }


    public CustomTextViewWarp(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (!isInEditMode()) {
            TypedArray styles_array = context.obtainStyledAttributes(attrs, R.styleable.CustomTextWarp);
            font = styles_array.getString(R.styleable.CustomTextWarp_font);
            setFont();

        }
    }
}


