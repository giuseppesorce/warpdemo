package it.warpmobile.demo.ui.widgets.progress;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Paint.Style;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

public class CustomCircleWaiting extends View {

	// dimensioni di default
	private int fRadius = 100;
	private int circleRadius = 80;
	private int barLength = 60;
	private int barWidth = 20;
	private int rimWidth = 20;
	private int textSize = 20;

	// Padding default 
	private int paddingTop = 5;
	private int paddingBottom = 5;
	private int paddingLeft = 5;
	private int paddingRight = 5;

	// Colori di default
	private int barColor = 0xAA042259;
	private int circleColor = 0x00000000;
	private int rimColor = 0xAADDDDDD;
	private int textColor = 0xFF000000;

	// Paints
	private Paint barPaint = new Paint();
	private Paint circlePaint = new Paint();
	private Paint rimPaint = new Paint();
	private Paint textPaint = new Paint();

	// Rectangles
	@SuppressWarnings("unused")
	private RectF rectBounds = new RectF();
	private RectF circleBounds = new RectF();

	private int spinSpeed = 5;

	
	private int delayMillis = 0;
	private Handler spinHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			invalidate();
			if (isSpinning) {
				progress += spinSpeed;
				if (progress > 360) {
					progress = 0;
				}
				spinHandler.sendEmptyMessageDelayed(0, delayMillis);
			}
			// super.handleMessage(msg);
		}
	};
	int progress = 0;
	boolean isSpinning = false;

	public CustomCircleWaiting(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

	}

	public CustomCircleWaiting(Context context) {
		super(context);
		rimColor = 0x90ffffff;
	}
	
	public CustomCircleWaiting(Context context, int col) {
		super(context);
		rimColor = col;
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		setupBounds();
		setupPaints();
		invalidate();
	}

	private void setupBounds() {
		paddingTop = this.getPaddingTop();
		paddingBottom = this.getPaddingBottom();
		paddingLeft = this.getPaddingLeft();
		paddingRight = this.getPaddingRight();

		rectBounds = new RectF(paddingLeft, paddingTop,
				this.getLayoutParams().width - paddingRight,
				this.getLayoutParams().height - paddingBottom);

		circleBounds = new RectF(paddingLeft + barWidth, paddingTop + barWidth,
				this.getLayoutParams().width - paddingRight - barWidth,
				this.getLayoutParams().height - paddingBottom - barWidth);

		fRadius = (this.getLayoutParams().width - paddingRight - barWidth) / 2;
		circleRadius = (fRadius - barWidth) + 1;
	}

	private void setupPaints() {
		barPaint.setColor(barColor);
		barPaint.setAntiAlias(true);
		barPaint.setStyle(Style.STROKE);
		barPaint.setStrokeWidth(barWidth);

		rimPaint.setColor(rimColor);
		rimPaint.setAntiAlias(true);
		rimPaint.setStyle(Style.STROKE);
		rimPaint.setStrokeWidth(rimWidth);

		circlePaint.setColor(circleColor);
		circlePaint.setAntiAlias(true);
		circlePaint.setStyle(Style.FILL);

		textPaint.setColor(textColor);
		textPaint.setStyle(Style.FILL);
		textPaint.setAntiAlias(true);
		textPaint.setTextSize(textSize);
	}

	// ----------------------------------
	// Animation stuff
	// ----------------------------------

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// Draw the rim
		canvas.drawArc(circleBounds, 360, 360, false, rimPaint);

		if (isSpinning) {
			canvas.drawArc(circleBounds, progress - 90, barLength, false,
					barPaint);
		} else {
			canvas.drawArc(circleBounds, -90, progress, false, barPaint);
		}
		// Draw the inner circle
		canvas.drawCircle((circleBounds.width() / 2) + rimWidth + paddingLeft,
				(circleBounds.height() / 2) + rimWidth + paddingTop,
				circleRadius, circlePaint);

	}

	public void resetCount() {
		progress = 0;

		invalidate();
	}

	public void stopSpinning() {
		isSpinning = false;
		progress = 0;
		spinHandler.removeMessages(0);
	}

	public void spin(int speed) {
		spinSpeed = speed;
		isSpinning = true;
		spinHandler.sendEmptyMessage(0);
	}

	public void spin() {
		isSpinning = true;
		spinHandler.sendEmptyMessage(0);
	}


	public void setProgress(int i) {
		isSpinning = false;
		progress = i;
		spinHandler.sendEmptyMessage(0);
	}

	public int getCircleRadius() {
		return circleRadius;
	}

	public void setCircleRadius(int circleRadius) {
		this.circleRadius = circleRadius;
	}

	public int getBarLength() {
		return barLength;
	}

	public void setBarLength(int barLength) {
		this.barLength = barLength;
	}

	public int getBarWidth() {
		return barWidth;
	}

	public void setBarWidth(int barWidth) {
		this.barWidth = barWidth;
	}

	@Override
	public int getPaddingTop() {
		return paddingTop;
	}

	public void setPaddingTop(int paddingTop) {
		this.paddingTop = paddingTop;
	}

	@Override
	public int getPaddingBottom() {
		return paddingBottom;
	}

	public void setPaddingBottom(int paddingBottom) {
		this.paddingBottom = paddingBottom;
	}

	@Override
	public int getPaddingLeft() {
		return paddingLeft;
	}

	public void setPaddingLeft(int paddingLeft) {
		this.paddingLeft = paddingLeft;
	}

	@Override
	public int getPaddingRight() {
		return paddingRight;
	}

	public void setPaddingRight(int paddingRight) {
		this.paddingRight = paddingRight;
	}

	public int getBarColor() {
		return barColor;
	}

	public void setBarColor(int barColor) {
		this.barColor = barColor;
	}

	public int getCircleColor() {
		return circleColor;
	}

	public void setCircleColor(int circleColor) {
		this.circleColor = circleColor;
	}

	public int getRimColor() {
		return rimColor;
	}

	public void setRimColor(int rimColor) {
		this.rimColor = rimColor;
	}

	public Shader getRimShader() {
		return rimPaint.getShader();
	}

	public void setRimShader(Shader shader) {
		this.rimPaint.setShader(shader);
	}

	public int getTextColor() {
		return textColor;
	}

	public void setTextColor(int textColor) {
		this.textColor = textColor;
	}

	public int getSpinSpeed() {
		return spinSpeed;
	}

	public void setSpinSpeed(int spinSpeed) {
		this.spinSpeed = spinSpeed;
	}

	public int getRimWidth() {
		return rimWidth;
	}

	public void setRimWidth(int rimWidth) {
		this.rimWidth = rimWidth;
	}

	public int getDelayMillis() {
		return delayMillis;
	}

	public void setDelayMillis(int delayMillis) {
		this.delayMillis = delayMillis;
	}

}
