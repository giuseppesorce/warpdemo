package it.warpmobile.demo.ui.fragments;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import it.warpmobile.demo.R;
import it.warpmobile.demo.comm.Communication;
import it.warpmobile.demo.comm.listeners.CommListener;
import it.warpmobile.demo.comm.responses.ResponseGeneric;
import it.warpmobile.demo.comm.responses.ResponseVideoList;
import it.warpmobile.demo.configs.WarpConstants;
import it.warpmobile.demo.modeldata.CPError;
import it.warpmobile.demo.modeldata.VideoItem;
import it.warpmobile.demo.ui.activities.YoutubeActivity;

/**
 * Created by devandroid on 18/08/14.
 */
public class VideoListFragment extends LFragment {


    private Communication comm;
    private ListView lvVideos;

    private ArrayList<VideoItem> videos = new ArrayList<VideoItem>();
    private VideoListAdatper adapter;

    public VideoListFragment() {
        typeFrag = WarpConstants.FragmentsType.VIDEO_LIST;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        lvVideos = new ListView(getActivity());

        lvVideos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showVideo(videos.get(position));
            }
        });
        loadVideoList();
        return lvVideos;

    }

    private void showVideo(VideoItem videoitem) {
        Intent intent= new Intent(getActivity(), YoutubeActivity.class);
        intent.putExtra("video", videoitem);
        getActivity().startActivity(intent);
    }


    private void loadVideoList() {

        comm = new Communication(getActivity().getApplicationContext());

        comm.getVideoList(new CommListener() {
            @Override
            public void onGetData(ResponseGeneric response) {

                videos = ((ResponseVideoList) response).data.items;
                adapter = new VideoListAdatper(getActivity(), R.layout.itemvideo, videos);
                lvVideos.setAdapter(adapter);


            }



            @Override
            public void onError(CPError error) {

            }

            @Override
            public void onServerResponseError(CPError error) {

            }
        });
    }


    public class VideoListAdatper extends ArrayAdapter<VideoItem> {

        private int resource;
        private LayoutInflater inflater;
        private Context mycontext;

        public VideoListAdatper(Context context, int resourceId,
                                List<VideoItem> objects) {
            super(context, resourceId, objects);
            mycontext = context;
            resource = resourceId;

            inflater = LayoutInflater.from(context);
        }


        @Override
        public View getView(int position, View view, ViewGroup parent) {


            VideoItem item = getItem(position);
            ViewHolder vh;
            if (view == null) {
                vh = new ViewHolder();
                view = inflater.inflate(R.layout.itemvideo, null);
                vh.tvTitle = (TextView) view.findViewById(R.id.tw_post_title);
                vh.tvImage = (ImageView) view.findViewById(R.id.img_video_preview);

                view.setTag(vh);
            } else {
                vh = (ViewHolder) view.getTag();
            }


            vh.tvTitle.setText(item.title);


            Picasso.with(mycontext).load(item.thumbnail.hqDefault).into(vh.tvImage);


            return view;

        }

        class ViewHolder {
            TextView tvTitle;
            ImageView tvImage;

        }

    }

}




