package it.warpmobile.demo.ui.fragments;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import it.warpmobile.demo.R;
import it.warpmobile.demo.comm.Communication;
import it.warpmobile.demo.comm.listeners.CommListener;
import it.warpmobile.demo.comm.responses.ResponseGeneric;
import it.warpmobile.demo.comm.responses.ResponseVideoList;
import it.warpmobile.demo.configs.WarpConstants;
import it.warpmobile.demo.debug.Dbg;
import it.warpmobile.demo.modeldata.CPError;
import it.warpmobile.demo.modeldata.VideoItem;
import it.warpmobile.demo.ui.activities.YoutubeActivity;
import it.warpmobile.demo.ui.widgets.texts.CustomTextViewWarp;

/**
 * Created by devandroid on 18/08/14.
 */
public class HomeFragment extends Fragment {



    private CustomTextViewWarp tvHome;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        tvHome = new CustomTextViewWarp(getActivity());
        tvHome.setText("Home seleziona una voce dal DrawerMenu");
        tvHome.setPadding(20, 20, 20, 20);
        tvHome.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
        tvHome.setFont("roboto");
        return tvHome;

    }



}




