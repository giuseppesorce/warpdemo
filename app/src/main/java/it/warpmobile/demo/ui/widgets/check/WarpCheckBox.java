package it.warpmobile.demo.ui.widgets.check;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import it.warpmobile.demo.R;

public class WarpCheckBox extends View {
    private Paint paint1 = new Paint();
    private int color_select;
    private boolean checked = false;
    private int marginrecblu = 10;

    public WarpCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public WarpCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WarpCheckBox(Context context) {
        super(context);

    }

    private void init() {
        color_select = getContext().getResources().getColor(
                R.color.col_blu_footer);

    }

    public void onDraw(Canvas canvas) {


        //    public void drawRect(float left, float top, float right, float bottom,
        int width = canvas.getWidth();
        int height = canvas.getHeight();

        int width_blu = width - marginrecblu;
        int height_blu = height - marginrecblu;

        paint1.setColor(Color.WHITE);

        canvas.drawRect(0, 0, width, height, paint1);
        if (checked) {
            paint1.setColor(color_select);
            canvas.drawRect(marginrecblu, marginrecblu, width_blu, height_blu, paint1);
        }

    }

    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                setChecked(!checked);
                break;
            case MotionEvent.ACTION_CANCEL:

                break;
            case MotionEvent.ACTION_UP:

                break;
        }
        return true;
    }

    public boolean isChecked() {

        return checked;
    }

    public void setChecked(boolean check) {
        checked = check;
        invalidate();

    }

}
