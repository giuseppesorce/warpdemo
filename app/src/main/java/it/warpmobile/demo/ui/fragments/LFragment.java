package it.warpmobile.demo.ui.fragments;


import android.app.Fragment;

/**
 * Code by Giuseppe Sorce srl.  on 27/11/13.
 */
public class LFragment extends Fragment {
    protected int typeFrag;

    public Boolean setBack() {
        return true;
    }

    public void setContent(int t) {
        typeFrag = t;
    }


    public int getContent() {
        return typeFrag;
    }

}
