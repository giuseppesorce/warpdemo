package it.warpmobile.demo.ui.widgets.animations.colors;

import android.graphics.PorterDuff;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Giuseppe Sorce on 19/07/13.
 * This class enable button effect color to any view
 *
 */
public class ButtonEffectColor {


    public static void setColorButton(final View view, final int color){
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:

                        view.getBackground().setColorFilter(color, PorterDuff.Mode.OVERLAY);
                        view.invalidate();
                          break;
                    case MotionEvent.ACTION_CANCEL:

                        view.getBackground().clearColorFilter();
                        view.invalidate();

                        break;
                    case MotionEvent.ACTION_UP:

                        view.getBackground().clearColorFilter();
                        view.invalidate();

                        break;
                }
                return true;
            }
        });
    }

    public static void setColorButton(final View view, final int color, final PorterDuff.Mode mode){
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:

                        view.getBackground().setColorFilter(color, mode);
                        view.invalidate();
                        break;
                    case MotionEvent.ACTION_CANCEL:

                        view.getBackground().clearColorFilter();
                        view.invalidate();

                        break;
                    case MotionEvent.ACTION_UP:

                        view.getBackground().clearColorFilter();
                        view.invalidate();

                        break;
                }
                return true;
            }
        });
    }
}
