package it.warpmobile.demo.ui.activities;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


import it.warpmobile.demo.R;
import it.warpmobile.demo.adapters.LeftDrawerMenuAdatper;
import it.warpmobile.demo.modeldata.ItemDrawer;
import it.warpmobile.demo.ui.fragments.HomeFragment;
import it.warpmobile.demo.ui.fragments.VideoListFragment;


public class MyActivity extends FragmentActivity {

    private static final String LIST_FRAGMENT_TAG = "list_fragment";
    private static final String HOME_TAG = "home";

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        setTitle("");
        initDrawerMenu();
        createHome();
   }

    private void initDrawerMenu() {
        getActionBar().setDisplayShowCustomEnabled(true);
        getActionBar().setDisplayShowTitleEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        createDrawerMenu();
    }

    private void createDrawerMenu() {
        ArrayList<ItemDrawer> menu = new ArrayList<ItemDrawer>();

        menu.add(new ItemDrawer("Home", getResources().getDrawable(
                R.drawable.home_icon)));
        menu.add(new ItemDrawer("News", getResources().getDrawable(
                R.drawable.news)));
        menu.add(new ItemDrawer("Position & Maps", getResources().getDrawable(
                R.drawable.gps_icone)));
        menu.add(new ItemDrawer("Settings", getResources().getDrawable(
                R.drawable.settings_icon)));


        LeftDrawerMenuAdatper adapter = new LeftDrawerMenuAdatper(
                getApplicationContext(), R.layout.row, menu);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            public void onDrawerClosed(View view) {

                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

    }

    private void createHome() {

        Fragment f = getFragmentManager().findFragmentByTag(HOME_TAG);
        if (f != null) {
            //  getFragmentManager().popBackStack();
        } else {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.animator.slide_up,
                            R.animator.slide_down,
                            R.animator.slide_up,
                            R.animator.slide_down)
                    .replace(R.id.content_frame, HomeFragment.instantiate(this, HomeFragment.class.getName()),
                            HOME_TAG
                    ).addToBackStack(null).commit();
        }
    }

    private void selectItem(int position) {
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);

        switch (position) {
            case 0:
                createHome();
                break;
            case 1:
                createVideoList();
                break;
        }
    }

    private void createVideoList() {


        Fragment f = getFragmentManager().findFragmentByTag(LIST_FRAGMENT_TAG);
        if (f != null) {
            //  getFragmentManager().popBackStack();
        } else {
            getFragmentManager().beginTransaction()
                    .setCustomAnimations(R.animator.slide_up,
                            R.animator.slide_down,
                            R.animator.slide_up,
                            R.animator.slide_down)
                    .replace(R.id.content_frame, VideoListFragment.instantiate(this, VideoListFragment.class.getName()),
                            LIST_FRAGMENT_TAG
                    ).addToBackStack(null).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case android.R.id.home:

                openMenu();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openMenu() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {

            mDrawerLayout.closeDrawers();
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);

        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

 
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }


}
