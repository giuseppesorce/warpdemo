package it.warpmobile.demo.modeldata;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by devandroid on 18/08/14.
 */
public class VideoItem implements Serializable{

    public String id;
    public String title;
    public String description;
    public String updated;

    public Thumb thumbnail;

    @SerializedName("player")
    public Player player;


    public String viewCount;

    public class Thumb implements Serializable {
        public String sqDefault;
        public String hqDefault;
    }
    public class Player implements Serializable {

        @SerializedName("mobile")
        public String mobile;

    }
}
