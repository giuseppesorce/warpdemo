package it.warpmobile.demo.modeldata;

import android.graphics.drawable.Drawable;

/**
 * Created by devandroid on 17/08/14.
 */
public class ItemDrawer {

    private String title;
    private Drawable icon;

    public ItemDrawer(String tit, Drawable d) {
        title = tit;
        icon = d;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }
}
