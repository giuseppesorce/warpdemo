package it.warpmobile.demo.debug;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Dbg {
	public static String TAG = "warpmobile";
    private static TextView tvDebug;

    public static void p(Object m) {
		try {
			Log.i(TAG, m.toString());
		} catch (Exception e) {

		}

	}

	public static void p(String m, Boolean grave) {
		try {
			Log.i(TAG, m);
            if(grave){
                appendLog(DateApp.getStamp()+": "+ m.toString());
            }
		} catch (Exception e) {

		}

	}

	public static void p(String m) {

		try {
			Log.i(TAG, m);

		} catch (Exception e) {

		}
	}

	public static void p(List m) {
		Log.i(TAG, m.toString());

	}

	public static void e(String m) {
		Log.e(TAG, m.toString());

	}

    public static void appendLog(String text) {

        File folder= new File(String.valueOf(Environment.getExternalStorageDirectory()), "logadmin");

        File logFile = new File(folder, "log.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            }catch (IOException e){
                e.printStackTrace();
            }
        }else{
            long kb = logFile.length() / 1024;

            if(kb > 1000){
                File logFiletorename = new File(folder, "log_old_"+ DateApp.getStampFile()+"_.txt");
                logFile.renameTo(logFiletorename);
                try {
                    logFile.createNewFile();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }

        }
        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void toast(Context applicationContext, String s) {
        Toast.makeText(applicationContext, s, Toast.LENGTH_LONG).show();
    }

    public static void setText(TextView dbg) {
        tvDebug= dbg;
    }
}
