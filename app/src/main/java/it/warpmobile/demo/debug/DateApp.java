package it.warpmobile.demo.debug;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Giuseppe Sorce on 10/04/14 - AdminDevice
 */
public class DateApp {

    public static String getStamp() {
        String format = "dd-MM-yyyy HH:mm:ss";

        SimpleDateFormat sdf = new SimpleDateFormat(format);

        return sdf.format(new Date());
    }

    public static String getStampFile() {
        String format = "dd_MM_yyyy_HH_mm_ss";

        SimpleDateFormat sdf = new SimpleDateFormat(format);

        return sdf.format(new Date());
    }
}
