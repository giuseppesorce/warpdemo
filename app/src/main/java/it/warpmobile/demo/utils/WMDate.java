package it.warpmobile.demo.utils;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

@SuppressLint("SimpleDateFormat")
public class WMDate {


    public static String getTextDate(String dt) {
        String txtdate = dt;


        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date = null;

        try {
            date = simpledateformat.parse(dt);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        String mese = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ITALIAN);
        String newdate = "" + calendar.get(Calendar.DAY_OF_MONTH) + " " + SimpleRegExp.capitalize(mese) + " " + calendar.get(Calendar.YEAR);
        return newdate;

    }

    public static Date getDateStringYouTube(String dt) {

        SimpleDateFormat sformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date mydate = null;
        try {
            mydate = sformat.parse(dt);
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return mydate;
    }

    public static String getStringYouTube(String start) {
        String end = start.substring(0, start.indexOf(".")).replace("T", " ");
        return end;
    }

}
