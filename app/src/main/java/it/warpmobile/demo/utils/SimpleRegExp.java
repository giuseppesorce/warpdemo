package it.warpmobile.demo.utils;

public class SimpleRegExp {

	public static String getYoutubeId(String url){
		
		String video_id = url.split("v=")[1];
		int ampersandPosition = video_id.indexOf('&');
		if(ampersandPosition != -1) {
		  video_id = video_id.substring(0, ampersandPosition);
		}
		return video_id;
	}

    public static String capitalize(String line){
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }


}
